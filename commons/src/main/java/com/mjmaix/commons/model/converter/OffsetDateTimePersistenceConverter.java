/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.model.converter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.OffsetDateTime;

/**
 * Read. https://weblogs.java.net/blog/montanajava/archive/ 2014/06/17/using-java-8-datetime-classes-jpa
 * http://www.adam-bien.com/roller/abien/entry/new_java_8_date_and
 * <p/>
 *
 * @author mjabadilla
 */
@Converter(autoApply = true)
public class OffsetDateTimePersistenceConverter implements AttributeConverter<OffsetDateTime, String> {

	
	private static final Logger LOG = LogManager.getLogger(OffsetDateTimePersistenceConverter.class.getName());

	@Override
	public String convertToDatabaseColumn(OffsetDateTime entityValue) {
		LOG.trace("OffsetDateTimePersistenceConverter convertToDatabaseColumn");
		return String.valueOf(entityValue);
	}

	@Override
	public OffsetDateTime convertToEntityAttribute(String databaseValue) {
		LOG.trace("OffsetDateTimePersistenceConverter convertToEntityAttribute");
		return OffsetDateTime.parse(databaseValue);
	}

}
