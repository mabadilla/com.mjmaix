package com.mjmaix.commons.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@NamedQueries({
		@NamedQuery(
			name = "Users.findAll",
			query = "SELECT NEW com.mjmaix.commons.model.Users(u.id, u.version, u.username, u.createdAt, u.createdBy, u.updatedAt, u.updatedBy) from Users u"),
		@NamedQuery(name = "Users.findByUsername", query = "SELECT u from Users u where u.username = :username") })
public class Users extends BaseEntityAudit {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StringProperty password;
	private StringProperty passwordSalt;
	private StringProperty username;
	private List<com.mjmaix.commons.model.Roles> roles;

	public Users() {
	}

	public Users(Long id, Long version, String username, LocalDateTime createdAt, String createdBy,
			LocalDateTime updatedAt, String updatedBy) {
		super();
		this.setId(id);
		this.setVersion(version);
		this.setUsername(username);
		this.setCreatedAt(createdAt);
		this.setCreatedBy(createdBy);
		this.setUpdatedBy(updatedBy);
		this.setUpdatedAt(updatedAt);
	}

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "users_seq_gen")
	@SequenceGenerator(name = "users_seq_gen", sequenceName = "USERS_SEQ")
	@Column(name = "id", nullable = false)
	public Long getId() {
		return super.getId();
	}

	@Column(name = "password", insertable = true, updatable = true, length = 32, nullable = false)
	public String getPassword() {
		return passwordProperty().get();
	}

	@Column(name = "password_salt", insertable = true, updatable = true, length = 64, nullable = false)
	public String getPasswordSalt() {
		return passwordSaltProperty().get();
	}

	@Column(name = "username", insertable = true, updatable = false, length = 12, nullable = false)
	public String getUsername() {
		return usernameProperty().get();
	}

	public void setPassword(String password) {
		this.passwordProperty().set(password);
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSaltProperty().set(passwordSalt);
	}

	public void setUsername(String username) {
		this.usernameProperty().set(username);
	}

	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(
		name = "user_roles", joinColumns = { @JoinColumn(name = "username") },
		inverseJoinColumns = { @JoinColumn(name = "role_name") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "username", "role_name" }))
	public List<com.mjmaix.commons.model.Roles> getRoles() {
		return this.roles;
	}

	public void setRoles(List<com.mjmaix.commons.model.Roles> role) {
		this.roles = role;
	}

	public StringProperty usernameProperty() {
		if (null == username) {
			username = new SimpleStringProperty(this, "username");
		}
		return username;
	}

	public StringProperty passwordProperty() {
		if (null == password) {
			password = new SimpleStringProperty(this, "password");
		}
		return password;
	}

	public StringProperty passwordSaltProperty() {
		if (null == passwordSalt) {
			passwordSalt = new SimpleStringProperty(this, "passwordSalt");
		}
		return passwordSalt;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getId()).append(this.getUsername()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Users)) {
			return false;
		}
		Users other = (Users) obj;
		return new EqualsBuilder().append(this.getId(), other.getId()).append(this.getUsername(), other.getUsername())
				.isEquals();
	}

	@Override
	public String toString() {
		return "ID: " + this.getId().toString() + "\nusername: " + this.getUsername();
	}

}
