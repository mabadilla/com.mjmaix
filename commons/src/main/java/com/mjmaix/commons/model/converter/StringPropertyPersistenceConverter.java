/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.model.converter;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author mjabadilla
 */
@Converter(autoApply = true)
public class StringPropertyPersistenceConverter implements AttributeConverter<StringProperty, String> {

	
	private static final Logger LOG = LogManager.getLogger(StringPropertyPersistenceConverter.class.getName());

	@Override
	public String convertToDatabaseColumn(StringProperty attribute) {
		LOG.trace("StringPropertyPersistenceConverter convertToDatabaseColumn");
		return attribute.get();
	}

	@Override
	public StringProperty convertToEntityAttribute(String dbData) {
		LOG.trace("StringPropertyPersistenceConverter convertToEntityAttribute");
		return new SimpleStringProperty(dbData);
	}

}
