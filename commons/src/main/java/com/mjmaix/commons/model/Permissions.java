package com.mjmaix.commons.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "permissions")
@NamedQueries({ @NamedQuery(name = "Permissions.findAll", query = "SELECT p from Permissions p") })
public class Permissions extends BaseEntityAudit {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StringProperty permission;

	private List<com.mjmaix.commons.model.Roles> roles = new ArrayList<>();

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "permissions_seq_gen")
	@SequenceGenerator(name = "permissions_seq_gen", sequenceName = "PERMISSIONS_SEQ")
	@Column(name = "id", nullable = false)
	public Long getId() {
		return super.getId();
	}

	@Column(name = "permission", nullable = false, unique = true, updatable = false)
	public String getPermission() {
		return permissionProperty().get();
	}

	public void setPermission(String permission) {
		this.permissionProperty().set(permission);
	}

	public StringProperty permissionProperty() {
		if (null == permission) {
			permission = new SimpleStringProperty(this, "permission");
		}
		return permission;
	}

	@ManyToMany(mappedBy = "permissions", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	public List<com.mjmaix.commons.model.Roles> getRoles() {
		return roles;
	}

	public void setRoles(List<com.mjmaix.commons.model.Roles> roles) {
		this.roles = roles;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getId()).append(this.getPermission()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Permissions)) {
			return false;
		}
		Permissions other = (Permissions) obj;
		return new EqualsBuilder().append(this.getId(), other.getId())
				.append(this.getPermission(), other.getPermission()).isEquals();
	}

	@Override
	public String toString() {
		return "ID: " + this.getId().toString() + "\npermission: " + this.getPermission();
	}

}
