/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.model;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.Size;

import com.mjmaix.commons.model.converter.LocalDateTimePersistenceConverter;

/**
 * @author mjabadilla
 */
@MappedSuperclass
@Access(AccessType.PROPERTY)
public abstract class BaseEntityAudit extends BaseEntity {

	/**
	 * Update if changed are not portable to previous version
	 */
	private static final long serialVersionUID = 2L;

	private static final int MAX_USER_LENGTH = 20;

	private transient Users user;

	private LocalDateTime createdAt;
	private String createdBy;
	private LocalDateTime updatedAt;
	private String updatedBy;

	@Convert(converter = LocalDateTimePersistenceConverter.class)
	@Column(name = "created_at")
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime time) {
		this.createdAt = time;
	}

	@Size(max = MAX_USER_LENGTH)
	@Column(name = "created_by", length = MAX_USER_LENGTH)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String user) {
		this.createdBy = user;
	}

	@Convert(converter = LocalDateTimePersistenceConverter.class)
	@Column(name = "updated_at")
	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime time) {
		this.updatedAt = time;
	}

	@Size(max = MAX_USER_LENGTH)
	@Column(name = "updated_by", length = MAX_USER_LENGTH)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String time) {
		this.updatedBy = time;
	}

	@PrePersist
	public void setCreated() {
		if (null != user) {
			this.createdBy = user.getUsername();
		}
		this.createdAt = LocalDateTime.now();
	}

	@PreUpdate
	public void setUpdated() {
		if (null != user) {
			this.updatedBy = user.getUsername();
		}
		this.updatedAt = LocalDateTime.now();
	}

	public void setUser(Users user) {
		this.user = user;
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);

}
