package com.mjmaix.commons.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "roles", uniqueConstraints = @UniqueConstraint(columnNames = "role_name"))
@NamedQueries({ @NamedQuery(name = "Roles.findAll", query = "SELECT r FROM Roles r") })
public class Roles extends BaseEntityAudit {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StringProperty roleName;

	private List<com.mjmaix.commons.model.Permissions> permissions;

	private List<com.mjmaix.commons.model.Users> users = new ArrayList<>();

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "roles_seq_gen")
	@SequenceGenerator(name = "roles_seq_gen", sequenceName = "ROLES_SEQ")
	@Column(name = "id", nullable = false)
	public Long getId() {
		return super.getId();
	}

	@Column(name = "role_name", length = 32, nullable = false)
	public String getRoleName() {
		return roleNameProperty().get();
	}

	public void setRoleName(String roleName) {
		this.roleNameProperty().set(roleName);
	}

	public StringProperty roleNameProperty() {
		if (null == roleName) {
			roleName = new SimpleStringProperty(this, "roleName");
		}
		return roleName;
	}

	@ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	public List<com.mjmaix.commons.model.Users> getUsers() {
		return users;
	}

	public void setUsers(List<com.mjmaix.commons.model.Users> users) {
		this.users = users;
	}

	@JoinTable(
		name = "roles_permissions", joinColumns = { @JoinColumn(name = "roles") },
		inverseJoinColumns = { @JoinColumn(name = "permission") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "roles", "permission" }))
	public List<com.mjmaix.commons.model.Permissions> getPermissions() {
		return this.permissions;
	}

	public void setPermissions(List<com.mjmaix.commons.model.Permissions> permissions) {
		this.permissions = permissions;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getId()).append(this.getRoleName()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Roles)) {
			return false;
		}
		Roles other = (Roles) obj;
		return new EqualsBuilder().append(this.getId(), other.getId()).append(this.getRoleName(), other.getRoleName())
				.isEquals();
	}

	@Override
	public String toString() {
		return "ID: " + this.getId().toString() + "\nrolename: " + this.getRoleName();
	}

}
