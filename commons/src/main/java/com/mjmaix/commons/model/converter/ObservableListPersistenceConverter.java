/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.model.converter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mjabadilla
 */
@Converter(autoApply = true)
public class ObservableListPersistenceConverter implements AttributeConverter<ObservableList, List> {

	
	private static final Logger LOG = LogManager.getLogger(ObservableListPersistenceConverter.class.getName());

	@Override
	public List convertToDatabaseColumn(ObservableList attribute) {
		LOG.trace("ObservableListPersistenceConverter convertToDatabaseColumn");
		return new ArrayList<>(attribute);
	}

	@Override
	public ObservableList convertToEntityAttribute(List dbData) {
		LOG.trace("ObservableListPersistenceConverter convertToEntityAttribute");
		return FXCollections.observableArrayList(dbData);
	}

}
