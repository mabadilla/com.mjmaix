/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.model;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;

/**
 * Base class for entities.
 * <p/>
 *
 * @author mjabadilla
 * @version alpha
 */
@MappedSuperclass
@Access(AccessType.PROPERTY)
public abstract class BaseEntity implements Serializable {

	/**
	 * Update if changed are not portable to previous version
	 */
	private static final long serialVersionUID = 2L;

	private LongProperty id;
	private Long version;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="base_seq_gen")
	@SequenceGenerator(name="base_seq_gen", sequenceName="BASE_SEQ")
	@Column(name = "id", nullable = false)
	public Long getId() {
		if (id == null) {
			id = new SimpleLongProperty(this, "id");
		}
		return id.get();
	}

	public void setId(Long idNum) {
		if (this.id == null) {
			this.id = new SimpleLongProperty(this, "id", idNum);
		} else {
			this.id.set(idNum);
		}
	}

	public LongProperty idProperty() {
		if (id == null) {
			this.id = new SimpleLongProperty(this, "id");
		}

		return id;
	}

	@Column(name = "version")
	@Version
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long v) {
		this.version = v;
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);

	@Override
	public abstract String toString();

}
