package com.mjmaix.commons.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class ReportFormatUtil {
	private ReportFormatUtil() {
	}

	private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MMM dd, yyyy");

	public static String toString(final LocalDateTime time) {
		return time.format(dateTimeFormatter);
	}

	public static String toString(final LocalDate date) {
		return date.format(dateTimeFormatter);
	}

	public static String toString(final BigDecimal dateTime) {
		return BigDecimalUtil.valueOf(dateTime).toString();
	}
}
