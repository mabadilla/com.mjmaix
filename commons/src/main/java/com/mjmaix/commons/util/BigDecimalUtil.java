/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * @author luwi
 */
public final class BigDecimalUtil {

	private BigDecimalUtil() {
	}

	public static BigDecimal convert(final String value) {
		return new BigDecimal(Objects.toString(value, "0"));
	}

	public static BigDecimal convert(final Integer value) {
		return new BigDecimal(Objects.toString(value, "0"));
	}

	public static boolean gte(final BigDecimal a, final BigDecimal b) {
		return a.compareTo(b) >= 0;
	}

	public static boolean gt(final BigDecimal a, final BigDecimal b) {
		return a.compareTo(b) > 0;
	}

	public static boolean lte(final BigDecimal a, final BigDecimal b) {
		return a.compareTo(b) <= 0;
	}

	public static boolean lt(final BigDecimal a, final BigDecimal b) {
		return a.compareTo(b) < 0;
	}

	public static boolean eq(final BigDecimal a, final BigDecimal b) {
		return a.compareTo(b) == 0;
	}

	public static BigDecimal valueOf(BigDecimal value) {
		return value.setScale(2, RoundingMode.HALF_EVEN);
	}

	public static BigDecimal valueOf(double value) {
		return valueOf(BigDecimal.valueOf(value));
	}

}
