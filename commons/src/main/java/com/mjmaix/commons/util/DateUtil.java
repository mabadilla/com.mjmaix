/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Helper functions for handling dates. http://code.makery.ch/library/javafx-8-tutorial/part3/
 * <p/>
 *
 * @author Marco Jakob
 */
public final class DateUtil {

	private static final Logger LOG = LogManager.getLogger(DateUtil.class.getName());

	private DateUtil() {

	}

	/**
	 * 
	 * @param date
	 * @param toDateFormat
	 *            default value format is "MM-dd-yyyy"
	 * @return
	 */
	public static String format(final LocalDate date, String toDateFormat) {
		if (null == toDateFormat) {
			toDateFormat = "MM-dd-yyyy";
		}

		if (null == date) {
			return null;
		}
		return DateTimeFormatter.ofPattern(toDateFormat).format(date);
	}
	
	public static String format(final LocalDateTime dateTime, String toDateFormat) {
		if (null == toDateFormat) {
			toDateFormat = "MM-dd-yyyy hh:mm:ss a";
		}

		if (null == dateTime) {
			return null;
		}
		return DateTimeFormatter.ofPattern(toDateFormat).format(dateTime);
	}

	public static LocalDate parse(final String dateString, String fromDateFormat) {
		try {
			return LocalDate.parse(dateString, DateTimeFormatter.ofPattern(fromDateFormat));
		} catch (DateTimeParseException e) {
			LOG.warn(e.getMessage(), e);
			return null;
		}
	}

	public static boolean validDate(final String dateString, String fromDateFormat) {
		// Try to parse the String.
		return DateUtil.parse(dateString, fromDateFormat) != null;
	}

	public static boolean validateDateRange(final LocalDate startDate, final LocalDate endDate) {
		if (endDate == null) {
			return true;
		}
		return endDate.isAfter(startDate);
	}
}
