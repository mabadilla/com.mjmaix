/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.commons.util;

import org.junit.*;

import com.mjmaix.commons.util.BigDecimalUtil;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * @author CCSAdmin
 */
public class BigDecimalUtilTest {

	public BigDecimalUtilTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	
	@Test
	public void testGte() {
		System.out.println("gte");

		assertEquals(true, BigDecimalUtil.gte(BigDecimal.ZERO, BigDecimal.ZERO));
		assertEquals(false, BigDecimalUtil.gte(BigDecimal.ZERO, BigDecimal.TEN));
		assertEquals(true, BigDecimalUtil.gte(BigDecimal.ONE, BigDecimal.ZERO));
	}

	
	@Test
	public void testGt() {
		System.out.println("gt");

		assertEquals(false, BigDecimalUtil.gt(BigDecimal.ZERO, BigDecimal.ZERO));
		assertEquals(false, BigDecimalUtil.gt(BigDecimal.ZERO, BigDecimal.TEN));
		assertEquals(true, BigDecimalUtil.gt(BigDecimal.ONE, BigDecimal.ZERO));
	}

	
	@Test
	public void testLte() {
		System.out.println("lte");
		assertEquals(true, BigDecimalUtil.lte(BigDecimal.ZERO, BigDecimal.ZERO));
		assertEquals(true, BigDecimalUtil.lte(BigDecimal.ZERO, BigDecimal.TEN));
		assertEquals(false, BigDecimalUtil.lte(BigDecimal.ONE, BigDecimal.ZERO));
	}

	
	@Test
	public void testLt() {
		System.out.println("lt");
		assertEquals(false, BigDecimalUtil.lt(BigDecimal.ZERO, BigDecimal.ZERO));
		assertEquals(true, BigDecimalUtil.lt(BigDecimal.ZERO, BigDecimal.TEN));
		assertEquals(false, BigDecimalUtil.lt(BigDecimal.ONE, BigDecimal.ZERO));
	}

	
	@Test
	public void testEq() {
		System.out.println("eq");
		assertEquals(true, BigDecimalUtil.eq(BigDecimal.ZERO, BigDecimal.ZERO));
		assertEquals(false, BigDecimalUtil.eq(BigDecimal.ZERO, BigDecimal.TEN));
		assertEquals(false, BigDecimalUtil.eq(BigDecimal.ONE, BigDecimal.ZERO));
	}

}
