package com.mjmaix.security.realm;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PasswordUtilsTest {

	@Test
	public void test() {
		PasswordManager pManager = PasswordManager.getInstance();
		String expected = pManager.encryptPassword("PasswordForThisUser");
		System.out.println("Expected password:   " + expected);

		assertTrue(pManager.passwordsMatch("PasswordForThisUser", expected));
	}

}
