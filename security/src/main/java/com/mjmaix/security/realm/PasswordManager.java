package com.mjmaix.security.realm;

import java.util.Objects;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Sha256Hash;

public final class PasswordManager {

	private DefaultHashService HASH_SERVICE;
	private DefaultPasswordService PASSWORD_SERVICE;

	private PasswordManager() {
		HASH_SERVICE = new DefaultHashService();
		PASSWORD_SERVICE = new DefaultPasswordService();
		HASH_SERVICE.setHashIterations(Integer.valueOf(ShiroIni.getInstance().getHashIterations()));
		HASH_SERVICE.setHashAlgorithmName(Sha256Hash.ALGORITHM_NAME);
	}

	public String encryptPassword(String password) {

		PASSWORD_SERVICE.setHashService(HASH_SERVICE);
		String encryptedPassword = PASSWORD_SERVICE.encryptPassword(password);

		return encryptedPassword;
	}

	public boolean passwordsMatch(Object plaintext, String saved) {
		if (Objects.isNull(plaintext) || Objects.isNull(saved)) {
			return false;
		}
		return saved.equals(encryptPassword(plaintext.toString()));
	}

	public static PasswordManager getInstance() {
		return PasswordManagerHolder.INSTANCE;
	}

	private static class PasswordManagerHolder {

		private static final PasswordManager INSTANCE = new PasswordManager();
	}

}
