/*
 *
 * Copyright (C) 2015 Michael James L. Abadilla <mjmaix@gmail.com>
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package com.mjmaix.security.realm;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author mjabadilla
 */
public final class ShiroIni {

	private static final Logger LOGGER = LogManager.getLogger(ShiroIni.class.getName());

	public final int PH_LEGAL_AGE = 18;

	private final Properties prop = new Properties();

	private InputStream input = null;

	private ShiroIni() {
		try {
			input = getClass().getClassLoader().getResourceAsStream("shiro.ini");
			prop.load(input);

			LOGGER.info(prop.toString());

		} catch (IOException ex) {
			LOGGER.error(ex.getMessage(), ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException ex) {
					LOGGER.error(ex.getMessage(), ex);
				}
			}
		}
	}

	public static ShiroIni getInstance() {
		return ShiroIniHolder.INSTANCE;
	}

	public String getHashPrivateSalt() {
		return prop.getProperty("hashService.privateSalt");
	}

	public String getHashPublicSalt() {
		return prop.getProperty("hashService.generatePublicSalt");
	}
	
	public String getHashIterations() {
		return prop.getProperty("hashService.hashIterations");
	}

	private static class ShiroIniHolder {

		private static final ShiroIni INSTANCE = new ShiroIni();
	}

}
